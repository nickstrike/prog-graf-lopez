#include "Personaje.h"


Personaje::Personaje()
{
	if (!textura.loadFromFile("Sprite.png"));
	sprite.setTexture(textura);
	
}


Personaje::~Personaje()
{
}

sf::Sprite Personaje::getSprite()
{
	return sprite;
}



void Personaje::Movimiento()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		sprite.move(-0.1,0);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		sprite.move(0.1, 0);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		sprite.move(0, -0.1);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		sprite.move(0, 0.1);
}