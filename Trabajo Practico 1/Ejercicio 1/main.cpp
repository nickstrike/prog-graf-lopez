#include <iostream>
#include <SFML/Graphics.hpp>
#include "Personaje.h"
#ifdef DEBUG
#include <vld.h>
#endif

using namespace std;

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Game");
	Personaje* personaje = new Personaje();

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed
				||sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				window.close();
		}
		personaje->Movimiento();
		window.clear();
		window.draw(personaje->getSprite());
		window.display();

		
	}

	delete personaje;

	return 0;
}