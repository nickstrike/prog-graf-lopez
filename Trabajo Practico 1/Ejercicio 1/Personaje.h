#pragma once
#ifndef PERSONAJE_H
#define PERSONAJE_H
#include <SFML/Graphics.hpp>
#include <SFML\Window\Keyboard.hpp>

class Personaje
{
private:
	sf::Texture textura;
	sf::Sprite sprite;

public:
	Personaje();
	~Personaje();
	sf::Sprite getSprite();
	void Movimiento();
};

#endif